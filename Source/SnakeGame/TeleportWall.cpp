// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportWall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
// Sets default values
ATeleportWall::ATeleportWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeleportWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportWall::Interact(AActor* Interactor, bool bIsHead) {
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake)) {
		FVector MovementVector(ForceInitToZero);
		switch (Snake->LastMoveDirection) {
		case EMovementDirection::UP:
			MovementVector.X -= 1145;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X += 1145;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= 1145;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += 1145;
			break;
		}
		Snake->SnakeElements[0]->AddActorWorldOffset(MovementVector);
	}
}

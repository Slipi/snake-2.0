// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SpawnPowers.generated.h"

class ASpeedBonus;
class ADoubleLifes;
UCLASS()
class SNAKEGAME_API ASpawnPowers : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnPowers();

	UPROPERTY()
	int32 NumOfPowers;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpeedBonus> SpeedClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADoubleLifes> LifesClass;
	AActor* LastBonus;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void Spawn();
};

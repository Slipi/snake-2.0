// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPowers.h"
#include "SpeedBonus.h"
#include "DoubleLifes.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
// Sets default values
ASpawnPowers::ASpawnPowers()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NumOfPowers = 2;
}

// Called when the game starts or when spawned
void ASpawnPowers::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnPowers::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASpawnPowers::Spawn() {
	switch (FMath::RandRange(0, NumOfPowers - 1)) {
	case 0: 
		{
			FVector SpawnLocation = this->GetTargetLocation();
			SpawnLocation.Z += 50;
			FTransform NewTransform(SpawnLocation);
			TArray<AActor*> FoundActors;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnPowers::StaticClass(), FoundActors);
			for (int i = FoundActors.Num() - 1; i >= 0; i--) {
				if (IsValid(FoundActors[i])) {
					auto Spawner = Cast<ASpawnPowers>(FoundActors[i]);
					if (IsValid(Spawner->LastBonus)) Spawner->LastBonus->Destroy();
				}
			}
			LastBonus = GetWorld()->SpawnActor<ASpeedBonus>(SpeedClass, NewTransform);
			break; 
		}
	case 1:
		{
			FVector SpawnLocation = this->GetTargetLocation();
			SpawnLocation.Z += 50;
			FTransform NewTransform(SpawnLocation);
			TArray<AActor*> FoundActors;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnPowers::StaticClass(), FoundActors);
			for (int i = FoundActors.Num() - 1; i >= 0; i--) {
				if (IsValid(FoundActors[i])) {
					auto Spawner = Cast<ASpawnPowers>(FoundActors[i]);
					if (IsValid(Spawner->LastBonus)) Spawner->LastBonus->Destroy();
				}
			}
			LastBonus = GetWorld()->SpawnActor<ADoubleLifes>(LifesClass, NewTransform);
			break; 
		}
	}
}



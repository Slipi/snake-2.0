// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SpawnPowers.h"
#include "Kismet/GameplayStatics.h"
// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5;
	LastMoveDirection = EMovementDirection::DOWN;
	SnakeSize = 0;
	isLifesBoost = false;
	isSpeedBoost = false;
	toggleXY = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum) {
	if (isLifesBoost) ElementsNum++;
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, toggleXY, toggleXY);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElemntType();
		}
	}
	SnakeSize += ElementSize;
	if (SnakeSize % 10 == 0 && isLifesBoost == 0 && isSpeedBoost == 0) {
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnPowers::StaticClass(), FoundActors);
		auto Spawner = Cast<ASpawnPowers>(FoundActors[FMath::RandRange(0, FoundActors.Num() - 1)]);
		Spawner->Spawn();
	}
	if (ElementsNum == 5) toggleXY = 5000;
}

void ASnakeBase::Move() {
	if (isSpeedBoost || isLifesBoost) {
		TimesMove++;
		if (TimesMove == 10 / MovementSpeed && isSpeedBoost) {
			MovementSpeed += 0.1;
			SetActorTickInterval(MovementSpeed);
			isSpeedBoost = false;
			TimesMove = 0;
		}
		if (TimesMove == 10 / MovementSpeed && isLifesBoost) {
			isLifesBoost = false;
			TimesMove = 0;
		}
	}
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection) {
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"
#include "SnakeElementBase.h"
#include "Kismet/GameplayStatics.h"
#include "Wall.h"
// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead) {
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			int8 canSpawn = true;
			int32 x, y;
			TArray<AActor*> FoundActors;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWall::StaticClass(), FoundActors);
			while (canSpawn) {
				x = FMath::RandRange(0, 1008) - 270;
				y = FMath::RandRange(0, 1008) - 550;
				for (int i = 0; i < Snake->SnakeElements.Num(); ++i) {
					if (abs(Snake->SnakeElements[i]->GetTargetLocation().X - x) <= Snake->ElementSize / 2 ||
						abs(Snake->SnakeElements[i]->GetTargetLocation().Y - y) <= Snake->ElementSize / 2)
					{
						break;
					}
					if (i == Snake->SnakeElements.Num() - 1) {
						for (int i = 0; i < FoundActors.Num(); ++i) {
							if (abs(FoundActors[i]->GetTargetLocation().X - x) <= Snake->ElementSize ||
								abs(FoundActors[i]->GetTargetLocation().Y - y) <= Snake->ElementSize)
							{
								break;
							}
							if (i == FoundActors.Num() - 1) canSpawn = false;
						}
					}
				}
				
			}
			FVector NewLocation(x, y, 0);
			FTransform NewTransform(NewLocation);
			GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
			this->Destroy();
		}
	}
}
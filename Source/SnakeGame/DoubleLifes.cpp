// Fill out your copyright notice in the Description page of Project Settings.


#include "DoubleLifes.h"
#include "SnakeBase.h"
// Sets default values
ADoubleLifes::ADoubleLifes()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADoubleLifes::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoubleLifes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADoubleLifes::Interact(AActor* Interactor, bool bIsHead){
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->isLifesBoost = true;
			this->Destroy();
		}
	}
}

